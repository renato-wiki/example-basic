# renato - A simple, lightweight wiki platform - basic example

This is a basic example for using the [renato-wiki](https://gitlab.com/renato-wiki/core).

## License

The source code is licensed under [WTFPL](https://gitlab.com/renato-wiki/example-basic/blob/master/LICENSE).
