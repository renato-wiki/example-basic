<!--json::RENATO-META::{"title":"Nested Test Article"}-->
## Section 1

This is supposed to be a code block:

```java
private int add(int x, int y) {
  return x + y;
}
```

Isn't that *cool*?

## Go for it

Try yourself!

| Pros | Cons | Misc |
|-----:|:-----|:----:|
| Lorem | Ipsum | hello world |
| dolor sit amet | ![Footprints](https://imgs.xkcd.com/comics/footprints.png) | 1337 |
