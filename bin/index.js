"use strict";
/* eslint new-cap: off */

const path = require("path");
const Renato = require("@renato-wiki/core");
const express = require("express");

const app = express();
const wikiRouter = express.Router();
const apiWikiRouter = express.Router();

const wiki = new Renato(path.join(__dirname, "..", "renato-data"));

wiki
    .bindRoutes(wikiRouter, apiWikiRouter)
    .catch((err) => {
      console.error(err.stack);
      Renato
          .shutdown()
          .finally(() => process.exit(err.code || 1));
    });

process.on("SIGUSR1", () => wiki.buildIndex());

app.use("/api", apiWikiRouter);
app.use("/", wikiRouter);

app.listen(3000);
